#include <cstdlib>
#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>
#include "unistd.h"

using namespace std;

/*
* CONFIG
*
* gameVersion: current version of game, as string incase you have to put in alpha, beta, indev, etc
* lfDebugEnabled: if dbgPrint should print out the values of various variables
* fiveSecondDelayLength: the length of the various 5 second delays in the game, in seconds. Guess what it defaults to...
*/

const bool lfDebugEnabled = false;
const string gameVersion = "1.0";
string fiveSecondDelayLength = "3";
const bool noGameDelays = false;

struct Player {
	bool haveLockedBox;
	string name;
	bool hasWood;
	unsigned int age;
	bool hasTTASTools;
	bool hasTempleKey;
	bool hasTempleBall;
};


bool templeTombLoop;
bool gotoForestLoop;
Player thePlayer;

void doSleep(string length) {
	#ifdef _WIN32
	if (!noGameDelays) {
        string timeoutCall = "timeout /t ";
        timeoutCall += length;
        timeoutCall += " /nobreak > NUL";
		system(timeoutCall.c_str());
	}
	#endif
	#ifdef __linux__
	if (!noGameDelays) {
		string timeoutCall = "sleep ";
		timeoutCall += length;
		system(timeoutCall.c_str());
	}
	#endif
}
void dbgPrint(string toPrint) {
     if (lfDebugEnabled == true) {
        cout << "DEBUG: " << toPrint << "\n";
     }
}
int antiString(string input) {
    dbgPrint("input == " + input);
    char *p = const_cast<char*>(input.c_str());
    std::stringstream s_str(p);
    int i;
    s_str >> i;
    dbgPrint("i == " + i);
    if (!i) {
       cout << "i is invalid\n";
    }
    return i;
}


//provide a bool to disable prewritten death messages, so you can write your own
void die(bool noMsg) {
	#ifdef _WIN32
	system("PAUSE");
	#endif
	exit(EXIT_SUCCESS);
}
//default
void die() {
	cout << "You have died.\n";
	cout << "Next time, don't do that.\n";
	cout << "Goodbye\n";
	#ifdef _WIN32
	system("PAUSE");
	#endif
	exit(EXIT_SUCCESS);
}

void afterTempleEscape() {
	string ATEInput;
    cout << "Do you want to chop wood, to build a house?\n";
    cin >> ATEInput;
    if (ATEInput == "yes") {
        thePlayer.hasWood = true;
        cout << "\nSuccessfully chopped wood\n";
    doSleep(fiveSecondDelayLength);
    }
    if (thePlayer.hasWood == true) {
    	cout << "Building house...\n";
    	cout << "\nNight has fallen, and the monsters are trying to break down your walls, but they hold strong.\
    	 Ybiek notices your house, and apologizes for accidentally picking you up and putting you in this evil world.\
    	  He picks you up and takes you back to your home on earth. He awards you with a smaller member of his species, to have as a pet, and to get around in the air.\
    	  \n\nIn case you didn't notice, you won the game.";
    } else {
    	cout << "The monsters are getting more vicious, and you are having trouble defending yourself\n";
    	
    	cout << "You are overwhelmed by the amount of monsters\n";
    	doSleep(fiveSecondDelayLength);
    	cout << "You are surrounded, fighting 10 zombies at once, and one of them bites you.\n";
    	cout << "Overwhelmed by the pain of the bite, along with the fatigue from fighting, you fall to the ground.\n";
    	cout << "The zombies tear away at your brain, the giants rip you apart, and use your arms as drumsticks, and the undescribable cat-meets lion-meets human eats the rest\n";
    	die();
    }
    #ifdef _WIN32
	system("PAUSE");
	#endif
	exit(EXIT_SUCCESS);
}

void templeTombAfterScare() {
	string TTASInput;
	if (!thePlayer.hasTempleBall) {
		cout << "Because you didn't get the ball in the temple, you didn't notice a tripwire, and a trap killed you.";
		cout << "Hint: next time go left or right";
		die(true);
	}
	cout << "\nYou continue down the path leading to the center of the chamber. You roll a ball to set off all the traps, so you can avoid them. You arrive at the tomb chamber.\n";
	cout << "\nYou see a lever on the wall. Do you want to pull it?";
	cin >> TTASInput;
    if (TTASInput == "yes") {
		cout << "You fell into a spike trap.\n";
		die();
    }
        else {
		     cout << "\nYou missed an opportunity... to die. Good job!\n";
        }
	
	cout << "You pick up the tools on the rack. You now have a pickaxe, an axe, and a crystal sword.\n";
	thePlayer.hasTTASTools = true;
	
	cout << "You go out the other side of the tomb. No traps fire at you. You think: \"The ancients must've figured out that if I was in the tomb chamber, I must've avoided the first traps, and these are useless...\"\n";
	afterTempleEscape();
}
void templeRight() {
	cout << "\nYou find: a key.\n";
	thePlayer.hasTempleKey = true;
	if (thePlayer.haveLockedBox) {
		cout << "\nYou have found the contents of the box. It has a piece of paper inside, saying \"THE LEVER IS A TRAP\"\n\n";
	}
	cout << "You have returned to the entrance of the temple.\n\n";
	cout << "Which way do you want to go? (options: left right ahead)\n";
	return;
}
void templeLeft() {
    cout << "\nYou find: a ball made of a strange hard material\n";
    thePlayer.hasTempleBall = true;
	cout << "You have returned to the entrance of the temple.\n\n";
	cout << "Which way do you want to go? (options: left right ahead)\n";
    return;
}
void templeTomb() {
	string templeTombInput;
	templeTombLoop = false;

	cout << "You have entered the tomb of the ancient king, and you walk in. Around the middle of the tomb, you notice a blast of cold air on your cheek. You see an arrow stuck in the wall. You look on the other side of the tomb, and you see a blank wall. You decide that the tomb is either cursed, or someone did a very good job of booby-trapping it.\n\n";
	cout << "Do you want to escape, or continue on? ";
	cin >> templeTombInput;
	while (templeTombLoop == false) {
    	
    	
    	if (templeTombInput == "escape") {templeTombLoop = true; 
    		gotoForestLoop = false;
    		cout << "Which way do you want to go? (options: left right ahead)\n";
    		return;
    	}
    		else if (templeTombInput == "run") {
    			gotoForestLoop = false;
    			cout << "Which way do you want to go? (options: left right ahead)\n";
    			return;
    		}
    			else if (templeTombInput == "back") {
    				gotoForestLoop = false;
    				cout << "Which way do you want to go? (options: left right ahead)\n";
    				return;
    			}
    				else if (templeTombInput == "ahead") {templeTombAfterScare();}
    					else if (templeTombInput == "continue") {templeTombAfterScare();}
    						else if (templeTombInput == "go") {templeTombAfterScare();}
    							else {
    								cout << "Input is invalid, try again\n";
    								templeTombLoop = false;
    								cin >> templeTombInput;
    							}
    }
}


void gotoForest() {
	string gotoForestInput;
	gotoForestLoop = false;

	doSleep(fiveSecondDelayLength);
	cout << "\nYou have entered the forest. The monsters almost ate you, but you escaped them. On the way out of the forest, you find a locked box. Naturally, you don't have the key to said lock, because you lost all your stuff when you landed in the field.\n";
	thePlayer.haveLockedBox = true;
	cout << "\nYou have reached the temple.\n";
	cout << "You have received some information:\n\n";
	cout << "The grave of some ancient king is ahead. It probably is cursed, but you may also be able to get some very necessary equipment\n\n";
	cout << "Which way do you want to go? (options: left right ahead)\n";
	
	while (gotoForestLoop == false) {
    	cin >> gotoForestInput;
    
    	
    	if (gotoForestInput == "left") {templeLeft(); gotoForestInput = "";}
    		else if (gotoForestInput == "right") {templeRight(); gotoForestInput = "";}
    			else if (gotoForestInput == "forward") {gotoForestLoop = true; templeTomb();}
    				else if (gotoForestInput == "ahead") {gotoForestLoop = true; templeTomb();}
    					else if (gotoForestInput == "tomb") {gotoForestLoop = true; templeTomb();}
    						else if (gotoForestInput == "grave") {gotoForestLoop = true; templeTomb();}
    							else {
    								cout << "Input is invalid, try again\n";
    								gotoForestLoop = false;
    								cin >> gotoForestInput;
    							}
    }
	
	
}
void escapeForest() {
    cout << "You stayed in the field. After night fell, the monsters came out to eat you. You tried to run away, but you got tired, and the monsters enjoyed their human snack.\n";
    cout << "You were eaten.\n";
    die(true);
}

int main(int argc, char *argv[]) {

    
    bool inputValid = false;
    string ifForestEntry;
    string tmpAge;
    //age before verification
    int uvAge;

    #ifdef __linux__
    	cout << "Welcome to Evil World version " << gameVersion << " for Linux" << "\n";
    #elif _WIN32
    	cout << "Welcome to Evil World version " << gameVersion << " for Windows" << "\n";
    #endif
    cout << "(c) 2013 Lucas Fink\n\n";
    cout << "Please enter your name: ";
    cin >> thePlayer.name;
    cout << "Welcome, " << thePlayer.name << "\n";
    cout << "Your input for the questions must be all lowercase, or else it will say that it's invalid\n";
    cout << "\nPlease enter your age: ";
    


    bool temp = false;
    while (temp == false) {     
          cin.ignore();
          temp = true;
    }
    getline(cin, tmpAge);
    uvAge = antiString(tmpAge);
    dbgPrint("\nuvAge == " + uvAge);

    while (uvAge <= 1) {
    	cout << "Your age... seems legit. You may have entered letters, which means your age is interpreted as 0.\n";
    	cout << "Please enter a valid age: ";
    	getline(cin, tmpAge);
    	cout << "\n";
    	uvAge = antiString(tmpAge);
    }
    thePlayer.age = uvAge;
    cout << "Your name is " << thePlayer.name << ", and you're " << thePlayer.age << " years old, according to your input.\n\n";
    doSleep(fiveSecondDelayLength);
    cout << "You have awakened in a field.\n";
    cout << "You have no stuff on you. There is a forest nearby. There may be monsters in it.\n\n What do you want to do?\n";
    cout << "options: stay, go (into the forest)";
    getline(cin, ifForestEntry);
    while (inputValid == false) {
    	
    	if (ifForestEntry == "yes") {gotoForest(); return 0;}
    		else if (ifForestEntry == "go") {gotoForest(); return 0;}
    			else if (ifForestEntry == "forest") {gotoForest(); return 0;}
    				else if (ifForestEntry == "escape") {escapeForest(); return 0;}
    					else if (ifForestEntry == "stay") {escapeForest(); return 0;}
    						else if (ifForestEntry == "no") {escapeForest(); return 0;}
    							else {
    								
    								inputValid = false;
    								cin >> ifForestEntry;
    							}
    }
    
    #ifdef _WIN32
    system("PAUSE");
    #endif
    
}


